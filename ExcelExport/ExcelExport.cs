﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace ExcelExportService
{
    public class ExcelExport<T>
    {
        private readonly ExcelPackage _excelPackage;
        private readonly ExcelWorksheet _excelWorksheet;

        public ExcelExport()
        {
            _excelPackage = new ExcelPackage();
            _excelWorksheet = _excelPackage.Workbook.Worksheets.Add("Data");
        }

        public int LoadFromCollection(IEnumerable<T> collection)
        {
            var dataTable = ConvertToDataTable(collection);
            var lastRowIndex = _excelWorksheet.Dimension != null? _excelWorksheet.Dimension.End.Row:0;
            _excelWorksheet.Cells[lastRowIndex + 1, 1].LoadFromDataTable(dataTable, true);
            return _excelWorksheet.Dimension.Rows;
        }

        public void AddElement(T item)
        {
            var lastRowIndex = _excelWorksheet.Dimension != null ? _excelWorksheet.Dimension.End.Row : 0;
            _excelWorksheet.Cells[lastRowIndex + 1 , 1].LoadFromCollection(new List<T> { item });
        }

        public bool SaveToFile(string saveAsLocation, bool isColored = false, string headerColor = "#CCCCFF", string headerFontColor = "#FFFFFF")
        {
            try
            {
                _excelWorksheet.Cells.AutoFitColumns(0);
                if (isColored)
                {
                    using (ExcelRange rng = _excelWorksheet.Cells[1, 1, 1, _excelWorksheet.Dimension.Columns])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        rng.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(headerColor));
                        rng.Style.Font.Color.SetColor(ColorTranslator.FromHtml(headerFontColor));
                    }
                }

                _excelPackage.SaveAs(new FileInfo(saveAsLocation));
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                _excelPackage.Dispose();
            }
        }

        private DataTable ConvertToDataTable(IEnumerable<T> data)
        {
            PropertyInfo[] properties = typeof(T).GetProperties();
            var table = new System.Data.DataTable();
            foreach (var prop in properties)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                if (!attrs.Any(a => a is IgnoreDataMemberAttribute))
                {

                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }
            }

            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (var prop in properties)
                {
                    object[] attrs = prop.GetCustomAttributes(true);
                    if (!attrs.Any(a => a is IgnoreDataMemberAttribute))
                    {
                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                    }
                }

                table.Rows.Add(row);
            }
            return table;

        }
    }
}
