﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExcelExportService;

namespace ConsoleTest
{

    class Program
    {
        static void Main(string[] args)
        {
            List<Emp> list = GetEmpList();

            var exp = new ExcelExport<Emp>();
            
            var rows = exp.LoadFromCollection(list);
            exp.AddElement(new Emp { City = "Tel Aviv", Empid = -5, Empname = "Test" });

            exp.SaveToFile("D:\\3.xlsx");
        }

        public class Emp
        {

            public int Empid { get; set; }
            public string Empname { get; set; }
            public string City { get; set; }


        }

        static List<Emp> GetEmpList()
        {
            List<Emp> list = new List<Emp>();

            for (int i = 0; i < 100000; i++)
            {
                list.Add(new Emp
                {
                    Empid = i,
                    Empname = $"Emp {i} Name",
                    City = $"Emp {i} City"
                });
            }


           return list;

        }
    }
}

